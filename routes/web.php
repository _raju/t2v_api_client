<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login
use App\Configuration;

//login
Route::get('/', 'AuthenticateController@loginForm')->name('login_form');
Route::get('/login-form', 'AuthenticateController@loginForm')->name('login_form');
Route::post('/login-form-submit', 'AuthenticateController@loginSubmit')->name('login_submit');

//signup
Route::get('/signup-form', 'AuthenticateController@signupForm')->name('signup_form');
Route::post('/signup-submit', 'AuthenticateController@signupSubmit')->name('signup_submit');


Route::group(['middleware' => ['web', 'apiTokenVerified']], function () {

    //dashboard
    Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');

    //update profile
    Route::post('/profile-update', 'ProfileController@profileUpdate')->name('profile_update');

    //logout
    Route::post('/logout', 'AuthenticateController@logout')->name('logout');

});
