<?php

namespace App\Http\Controllers;

use App\AppApiReqRes;
use App\Configuration;
use App\Service\ApiTokenService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthenticateController extends Controller
{
    use ApiTokenService;

    /**
     * @return \Illuminate\View\View
     */
    public function signupForm()
    {
        return view('signup-form');
    }


    /**
     * @return \Illuminate\View\View
     */
    public function loginForm()
    {
        return view('login-form');
    }


    /**
     * User sign up
     *
     * @param Request $request
     * @return \Illuminate\View\View
     * @return string
     */
    public function signupSubmit(Request $request)
    {
        // input validation
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ];

        $request->validate($rules);

        // token request
        $token_response = json_decode($this->getToken());

        if ($token_response->responseCode == 0) {
            return redirect()->back()->withInput()->with('error', $token_response->msg);
        }

        try {
            $api_request_url = config('app.tech_2_view_signup_url');

            DB::beginTransaction();

            $api_req_res = new AppApiReqRes();
            $api_req_res->status = 2; // 2 = data-request sending
            $api_req_res->data_request_json = json_encode([
                'api_request_url' => $api_request_url,
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password
            ]);
            $api_req_res->save();

            // curl start
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $api_request_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('name' => $request->name, 'email' => $request->email, 'password' => $request->password),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $token_response->data"
                ),
            ));

            $response = curl_exec($curl);

            if (curl_errno($curl)) {
                curl_close($curl);

                return redirect()->back()->withInput()->with('error', curl_error($curl));
            }

            curl_close($curl);
            // curl end

            // response processing
            $decoded_response = json_decode($response);
            $response_code = $decoded_response->tech2ViewResponse->responseCode;

            if ($response_code == 200) {
                $api_req_res->status = 1; // success

                // create new user
                $user = new User();
                $user->name = $decoded_response->tech2ViewResponse->responseData->name;
                $user->email = $decoded_response->tech2ViewResponse->responseData->email;
                $user->password = Hash::make($decoded_response->tech2ViewResponse->responseData->password);
                $user->save();

                Auth::loginUsingId($user->id);

            } elseif (in_array($response_code, [400, 401])) {
                $api_req_res->status = -1; // validation failed
            } elseif ($response_code == 500) {
                $api_req_res->status = -3; // unreachable
            } else {
                $api_req_res->status = -9; // unknown error
            }

            $api_req_res->response_code = $response_code;
            $api_req_res->data_response_json = $response;
            $api_req_res->response_msg = json_encode($decoded_response->tech2ViewResponse->message);
            $api_req_res->save();

            DB::commit();

            $message = $api_req_res->response_msg;

        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
        }

        // successfully sign-up & redirect to dashboard
        if (Auth::check()) {
            return redirect()->route('dashboard')->with('message', 'Successfully sign-up.');
        }

        return redirect()->back()->withInput()->with('error', $message);
    }


    /**
     * User login
     *
     * @param Request $request
     * @return \Illuminate\View\View
     * @return string
     */
    public function loginSubmit(Request $request)
    {
        // input validation
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        $request->validate($rules);

        // token request
        $token_response = json_decode($this->getToken());

        if ($token_response->responseCode == 0) {
            return redirect()->back()->withInput()->with('error', $token_response->msg);
        }

        try {
            $api_request_url = config('app.tech_2_view_login_url');

            DB::beginTransaction();

            $api_req_res = new AppApiReqRes();
            $api_req_res->status = 2; // 2 = data-request sending
            $api_req_res->data_request_json = json_encode([
                'api_request_url' => $api_request_url,
                'email' => $request->email,
                'password' => $request->password
            ]);
            $api_req_res->save();

            // curl start
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $api_request_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('email' => $request->email, 'password' => $request->password),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $token_response->data"
                ),
            ));

            $response = curl_exec($curl);

            if (curl_errno($curl)) {
                curl_close($curl);

                return redirect()->back()->withInput()->with('error', curl_error($curl));
            }

            curl_close($curl);
            // curl end

            // response processing
            $decoded_response = json_decode($response);
            $response_code = $decoded_response->tech2ViewResponse->responseCode;

            if ($response_code == 200) {
                $api_req_res->status = 1; // success

                // create or update user
                $user = User::firstOrNew(['email' => $decoded_response->tech2ViewResponse->responseData->email]);
                $user->password = Hash::make($decoded_response->tech2ViewResponse->responseData->password);
                $user->save();

                Auth::loginUsingId($user->id);

            } elseif (in_array($response_code, [400, 401])) {
                $api_req_res->status = -1; // validation failed
            } elseif ($response_code == 500) {
                $api_req_res->status = -3; // unreachable
            } else {
                $api_req_res->status = -9; // unknown error
            }

            $api_req_res->response_code = $response_code;
            $api_req_res->data_response_json = $response;
            $api_req_res->response_msg = json_encode($decoded_response->tech2ViewResponse->message);
            $api_req_res->save();

            DB::commit();

            $message = $api_req_res->response_msg;

        } catch (\Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
        }

        // successfully login & redirect to dashboard
        if (Auth::check()) {
            return redirect()->route('dashboard')->with('message', 'Successfully Login.');
        }

        return redirect()->back()->withInput()->with('error', $message);
    }

    /**
     * User logout
     *
     * @return \Illuminate\View\View
     * @return string
     */
    public function logout()
    {
        try {
            Configuration::where(['caption' => 'TECH_2_VIEW_API_TOKEN'])
                ->update([
                    'value' => '', 'value2' => ''
                ]);
            Auth::logout();

            return redirect()->route('login_form')->with('message', 'Successfully logout.');

        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
