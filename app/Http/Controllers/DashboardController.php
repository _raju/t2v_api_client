<?php

namespace App\Http\Controllers;


class DashboardController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function dashboard()
    {
        return view('dashboard');
    }
}
