<?php

namespace App\Http\Middleware;

use App\Configuration;
use Closure;

class ApiTokenVerified
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_token = Configuration::where(['caption' => 'TECH_2_VIEW_API_TOKEN'])->first(['value', 'value2']);

        if ($api_token->value && $api_token->value2 && $api_token->value2 > time()) {
            return $next($request);
        }
        return redirect('/')->with('message', 'Token validation error!');
    }
}
