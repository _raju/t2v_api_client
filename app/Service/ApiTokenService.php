<?php


namespace App\Service;

use App\Configuration;
use App\TokenReqRes;
use Carbon\Carbon;

trait ApiTokenService
{
    /**
     * @return false|string
     */
    private function getToken()
    {
        $api_token = Configuration::firstOrNew(['caption' => 'TECH_2_VIEW_API_TOKEN']);

        if ($api_token->value && $api_token->value2 && $api_token->value2 > time()) {
            return json_encode(['responseCode' => 1, 'data' => $api_token->value]);
        }

        // api credentials
        $api_token_url = config('app.tech_2_view_token_url');
        $grant_type = config('app.tech_2_view_grant_type');
        $client_id = config('app.tech_2_view_client_id');
        $secret_key = config('app.tech_2_view_secret_key');

        $token_req_res = new TokenReqRes();
        $token_req_res->status = 1; // request for token
        $token_req_res->data_request_json = json_encode([
            'api_token_url' => $api_token_url,
            'grant_type' => $grant_type,
            'client_id' => $client_id,
            'client_secret_key' => $secret_key
        ]);

        $token_req_res->save();

        try {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $api_token_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array(
                    'grant_type' => $grant_type,
                    'client_id' => $client_id,
                    'client_secret_key' => $secret_key
                ),
            ));

            $response = curl_exec($curl);

            if (curl_errno($curl)) {
                curl_close($curl);

                return json_encode(['responseCode' => 0, 'msg' => curl_error($curl), 'data' => '']);
            }

            curl_close($curl);

            $decoded_data = json_decode($response)->tech2ViewResponse->responseData;

            $token_req_res->status = 2; // response gotten for token
            $token_req_res->data_response_json = $response;
            $token_req_res->save();

            if (!$response || !property_exists($decoded_data, 'token')) {
                return json_encode(['responseCode' => 0, 'msg' => 'Tech2View API connection failed!', 'data' => '']);
            }

            // updating token
            $api_token->value = $decoded_data->token;
            $api_token->value2 = (time() + $decoded_data->expire_in) - 60;// deducted for latency
            $api_token->updated_at = Carbon::now();
            $api_token->save();

            return json_encode(['responseCode' => 1, 'data' => $decoded_data->token]);

        } catch (\Exception $e) {
            return json_encode(['responseCode' => 0, 'msg' => $e->getMessage() . $e->getFile() . $e->getLine(), 'data' => '']);
        }
    }
}
