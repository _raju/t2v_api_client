<?php


namespace App;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class AppApiReqRes extends Model
{
    protected $table = 'api_req_res';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'token_request_json',
        'token_response_json',
        'status	',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public static function boot()
    {
        parent::boot();
        // Before update
        static::creating(function ($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function ($post) {
            $post->updated_by = CommonFunction::getUserId();
        });

    }
}
