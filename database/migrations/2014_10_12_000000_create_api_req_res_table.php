<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiReqResTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_req_res', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('data_request_json')->nullable();
            $table->text('data_response_json')->nullable();
            $table->string('response_code', 50)->nullable();
            $table->string('response_msg', 100)->nullable();
            $table->tinyInteger('status')->default('1')->comment('0= initiate, 1= success, 2=data-request sending, -1= validation failed, -2= unreachable, -9=unknown error');
            $table->timestamps();
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_req_res');
    }
}
