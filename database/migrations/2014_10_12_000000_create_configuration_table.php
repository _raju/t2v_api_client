<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuration', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('caption');
            $table->text('value');
            $table->string('details')->nullable();
            $table->string('value2', 100)->nullable();
            $table->timestamps();
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
        });

        // Insert data
        DB::table('configuration')->insert(
            array(
                'caption' => 'TECH_2_VIEW_API_TOKEN',
                'value' => '',
                'details' => 'store token to prevent repeated hit to server for generation token',
                'value2' => '',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'created_by' => 0,
                'updated_by' => 0
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuration');
    }
}
