<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokenReqResTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token_req_res', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('data_request_json')->nullable();
            $table->text('data_response_json')->nullable();
            $table->tinyInteger('status')->default('1')->comment('1= request for token, 2 = response gotten for token');
            $table->timestamps();
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('token_req_res');
    }
}
