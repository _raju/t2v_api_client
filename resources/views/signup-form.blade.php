<!doctype html>
<html lang="en">
<head>
    <title>Sign Up</title>
    <meta name="viewport" content="width = device-width, initial-scale = 1.0">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>

    <style>
        fieldset {
            border: 1px solid #ddd !important;
            margin: 0;
            xmin-width: 0;
            padding: 10px;
            position: relative;
            border-radius: 4px;
            background-color: #f5f5f5;
            padding-left: 10px !important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="container-fluid">

        {{--Flash message--}}
        @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ session()->get('error') }}
            </div>
        @endif
        {{--Flash message--}}

        <div class="panel panel-info">
            <div class="panel-heading">
                <span style="font-size: 17px"> Signup Form</span>
            </div>

            <div class="panel-body">
                <fieldset>
                    <form method="POST" action="{{ route('signup_submit') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="name" class="form-control" id="exampleInputName1"
                                   aria-describedby="emailHelp" value="{{ old('name') }}"
                                   placeholder="Enter name">
                            {!! $errors->first('name','<span class="help-block text-danger">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp" value="{{ old('email') }}"
                                   placeholder="Enter email">
                            {!! $errors->first('email','<span class="help-block text-danger">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                                   value="{{ old('password') }}" placeholder="Password">
                            {!! $errors->first('password','<span class="help-block text-danger">:message</span>') !!}
                        </div>

                        <span style="display: inline-block;">
                           <button type="submit" class="btn btn-info">Signup</button>
                        </span>
                        <span style="display: inline-block;float: right">
                            <a href="{{ route('login_form') }}" style="font-size: 16px">
                                <strong><u>Login</u></strong>
                            </a>
                        </span>

                    </form>
                </fieldset>
            </div>
        </div>

    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

</body>
</html>
