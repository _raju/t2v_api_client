<!DOCTYPE html>
<html>

<head>
    <title>Dashboard</title>
    <meta name="viewport" content="width = device-width, initial-scale = 1.0">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        fieldset {
            border: 1px solid #ddd !important;
            margin: 0;
            xmin-width: 0;
            padding: 10px;
            position: relative;
            border-radius: 4px;
            background-color: #f5f5f5;
            padding-left: 10px !important;
        }
    </style>

</head>

<body>

<div class="panel panel-info">
    <div class="panel-heading">
        <span style="display: inline-block;">
            <span style="font-size: 17px"> Dashboard</span>
        </span>
        <span style="display: inline-block;float: right">
           <form action="{{ route('logout') }}" method="POST">
               @csrf
               <button>Logout</button>
           </form>
        </span>
    </div>
</div>

<div class="container">
    <div class="container-fluid">

        {{--Flash message--}}
        @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {{ session()->get('error') }}
            </div>
        @endif
        {{--Flash message--}}

        <div class="panel panel-info">
            <div class="panel-heading">
                <span style="font-size: 17px"> User Info</span>
            </div>

            <div class="panel-body">
                <fieldset>
                    <form method="POST" action="{{ route('profile_update') }}">
                        @csrf
                        <input type="hidden" name="email" value="{{ $user_email }}">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="name" class="form-control" id="exampleInputName1"
                                   aria-describedby="emailHelp" value="{{ $user_name }}"
                                   placeholder="Enter name">
                            {!! $errors->first('name','<span class="help-block text-danger">:message</span>') !!}
                        </div>
                        <button type="submit" class="btn btn-info">Update</button>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

</body>
</html>
